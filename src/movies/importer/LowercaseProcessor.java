package movies.importer;
import java.util.ArrayList;
//Julian Mora

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,true);
	}
	
	public ArrayList<String> process(ArrayList<String> inputArray){
		ArrayList<String> asLower = new ArrayList<String>();
		for(String i: inputArray) {
			asLower.add(i.toLowerCase());
		}
		return asLower;
	}
	
}
