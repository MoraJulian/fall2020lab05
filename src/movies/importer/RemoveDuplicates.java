package movies.importer;
import java.util.ArrayList;
//Julian Mora

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,false);
	}
	
	
	public ArrayList<String> process(ArrayList<String> inputArray){
		ArrayList<String> noDuplicate = new ArrayList<String>();
		String stringSearched;
		for(int i = 0;i<inputArray.size();i++) {
			stringSearched = inputArray.get(i);
			if(!noDuplicate.contains(stringSearched)) {
				noDuplicate.add(stringSearched);
			}
		}
		return noDuplicate;
	}

}
